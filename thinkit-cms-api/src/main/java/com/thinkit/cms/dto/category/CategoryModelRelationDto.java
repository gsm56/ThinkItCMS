package com.thinkit.cms.dto.category;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 分类选择模型时关系表 该模型值 tk_model 表 不是 分类模型表
 * </p>
 *
 * @author LG
 * @since 2020-08-10
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryModelRelationDto extends BaseDto {


        private String siteId;


        private String modelId;


        @NotBlank(message = "分类ID不能为空,清先保存表单")
        private String categoryId;

        // @NotBlank(message = "模板ID不能为空!")
        private String templateId;

        private String templatePath;

}
