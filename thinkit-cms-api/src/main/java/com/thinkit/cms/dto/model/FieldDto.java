package com.thinkit.cms.dto.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.core.annotation.valid.ValidGroup1;
import com.thinkit.core.annotation.valid.ValidGroup2;
import com.thinkit.utils.model.BaseFieldDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 *
 * </p>
 *
 * @author LG
 * @since 2019-10-23
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FieldDto extends BaseFieldDto {

    private static final long serialVersionUID = 1L;

    /**
     * 模型名称
     */
    @NotBlank(message = "模型名称不能为空",groups = {ValidGroup1.class, ValidGroup2.class})
    private String name;

    /**
     * 静态化模板地址
     */
    @NotBlank(message = "模板地址不能为空",groups = {ValidGroup1.class, ValidGroup2.class})
    private String templatePath;

    /**
     * 模板ID
     */
    @NotBlank(message = "模板地址不能为空",groups = {ValidGroup1.class, ValidGroup2.class})
    private String templateId;

    private String md5Key;


   // @NotBlank(message = "模型模板地址不能为空",groups = {ValidGroup2.class})
    private String modelTemplateId;

    /**
     * 是否有用图片
     */
    private Boolean hasImages;

    private Boolean supportWehcat;

    private Boolean supportTops;

    /**
     * 是否拥有文件
     */
    private Boolean hasFiles;

    private Boolean isUrl;

}
