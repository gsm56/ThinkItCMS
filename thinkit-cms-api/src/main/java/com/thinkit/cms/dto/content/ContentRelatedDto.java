package com.thinkit.cms.dto.content;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;
/**
 * <p>
 * 内容推荐
 * </p>
 *
 * @author lg
 * @since 2020-09-30
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContentRelatedDto extends BaseDto {

        /**
        * 当前内容id
        */
        private String contentId;


        /**
        * 推荐的内容ID
        */
        private String relatedCid;


        /**
        * 排序
        */
        private Integer sort;


}
