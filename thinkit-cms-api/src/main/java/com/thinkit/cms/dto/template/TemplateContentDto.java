package com.thinkit.cms.dto.template;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 模板表
 * </p>
 *
 * @author lg
 * @since 2020-07-23
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TemplateContentDto extends BaseDto {

        private static final long serialVersionUID = 1L;

        /**
        * 内容
        */
        private String content;


        /**
         * 路径
         */
        @NotBlank(message = "路径不能为空")
        private String path;


}
