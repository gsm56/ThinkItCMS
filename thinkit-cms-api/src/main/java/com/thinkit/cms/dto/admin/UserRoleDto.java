package com.thinkit.cms.dto.admin;

import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户与角色对应关系
 * </p>
 *
 * @author dl
 * @since 2018-03-23
 */
@Data
@Accessors(chain = true)
public class UserRoleDto extends BaseDto {

    private static final long serialVersionUID = 1L;


    /**
     * 用户ID
     */
    private String userId;

    /**
     * 角色ID
     */
    private String roleId;



}
