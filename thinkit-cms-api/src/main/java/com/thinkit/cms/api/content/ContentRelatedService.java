package com.thinkit.cms.api.content;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.content.ContentRelatedDto;
import com.thinkit.core.base.BaseService;

import java.util.List;

/**
 * <p>
 * 内容推荐 服务类
 * </p>
 *
 * @author lg
 * @since 2020-09-30
 */
public interface ContentRelatedService extends BaseService<ContentRelatedDto> {


    void saveRelatedBath(List<ContentRelatedDto> dtos);

    /**
     * 查询相关推荐
     * @param contentId
     * @return
     */
    List<ContentDto> listRelated(String contentId);
}