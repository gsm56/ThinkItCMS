package com.thinkit.cms.api.admin;
import com.thinkit.cms.dto.admin.MemberDto;
import com.thinkit.core.base.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lg
 * @since 2020-11-03
 */
public interface MemberService extends BaseService<MemberDto> {


    MemberDto loadMemUserByUsername(String userAccount);
}