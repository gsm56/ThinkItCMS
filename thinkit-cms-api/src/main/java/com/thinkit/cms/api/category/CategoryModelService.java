package com.thinkit.cms.api.category;
import com.thinkit.cms.dto.category.CategoryModelDto;
import com.thinkit.core.base.BaseService;

import java.util.List;

/**
 * <p>
 * 分类扩展模型 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-08
 */
public interface CategoryModelService extends BaseService<CategoryModelDto> {


    boolean delete(String id);

    List<CategoryModelDto> loadModels(CategoryModelDto v);

    String getDesignField(String id);
}