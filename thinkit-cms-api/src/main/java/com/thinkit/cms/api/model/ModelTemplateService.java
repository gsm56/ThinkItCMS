package com.thinkit.cms.api.model;

import com.thinkit.cms.dto.model.ModelTemplateDto;
import com.thinkit.core.base.BaseService;

/**
 * <p>
 * 模型模板针对不同的站点不同的模型下有不同的 模板页面 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
public interface ModelTemplateService extends BaseService<ModelTemplateDto> {


    void saveModelTemp(String modelId, String siteId, String templateId, String templatePath);

    ModelTemplateDto getModelTemplate(String modelId, String templateId, String siteId);
}