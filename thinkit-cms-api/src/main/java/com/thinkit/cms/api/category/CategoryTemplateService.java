package com.thinkit.cms.api.category;

import com.thinkit.cms.dto.category.CategoryTemplateDto;
import com.thinkit.core.base.BaseService;

/**
 * <p>
 * 分类-模板配置表 服务类
 * </p>
 *
 * @author LG
 * @since 2020-08-12
 */
public interface CategoryTemplateService extends BaseService<CategoryTemplateDto> {


    void updateTemplate(String categoryId, String siteId, String templateId, String templatePath);

    String getCategoryTempPath(String categoryId, String siteId, String templateId);

    CategoryTemplateDto getCategoryTemp(String categoryId, String siteId, String templateId);

    /**
     * 查询栏目对应模型的初始化模板
     * @param categoryId
     * @return
     */
    String getTempPathByCategoryId(String categoryId, String siteId);
}