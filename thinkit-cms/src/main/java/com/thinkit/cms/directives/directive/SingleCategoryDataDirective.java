package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 获取单页栏目内容详情
 */
@Component
public class SingleCategoryDataDirective extends BaseDirective {

    @Autowired
    ContentService contentService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
        String id=render.getString("id");
        String categoryId=render.getString("categoryId");
        if(Checker.BeBlank(id)){
            id = categoryId;
        }
        String code=render.getString("code");
        if(Checker.BeNotBlank(id) ||  Checker.BeNotBlank(code)){
            Map<String,Object> content = contentService.categorySingleData(id,code);
            if(Checker.BeNotNull(content)){
                render.putAll(content);
            }
            render.render();
        }
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.CMS_CATEGORY_SINGLE_DATA_DIRECTIVE;
    }
}
