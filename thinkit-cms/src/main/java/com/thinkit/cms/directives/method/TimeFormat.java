package com.thinkit.cms.directives.method;

import cn.hutool.core.date.DateUtil;
import com.thinkit.directive.emums.LangEnum;
import com.thinkit.directive.emums.MethodEnum;
import com.thinkit.directive.render.BaseMethod;
import com.thinkit.utils.utils.Checker;
import freemarker.template.TemplateModelException;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class TimeFormat extends BaseMethod {

    @Override
    public Object exec(List list) throws TemplateModelException {
        String defaultStype ="yyyy-MM-dd";
        Date date = getDate(0,list, LangEnum.DATE);
        if(Checker.BeNotNull(date)){
            String style = getString(1,list, LangEnum.STRING);
            if(Checker.BeNotBlank(style)){
                return DateUtil.format(date,style);
            }else{
                return DateUtil.format(date,defaultStype);
            }
        }
        return DateUtil.format(new Date(),defaultStype);
    }

    @Override
    public MethodEnum getName() {
        return MethodEnum.FORMAT;
    }
}
