package com.thinkit.cms.controller.strategy;

import com.thinkit.cms.api.strategy.StrategyService;
import com.thinkit.cms.dto.strategy.StrategyDto;
import com.thinkit.core.base.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lg
 * @since 2021-05-12
 */
@Validated
@RestController
@RequestMapping("strategy")
public class StrategyController extends BaseController<StrategyService> {


    @PostMapping(value="saveStrategy")
    public void saveStrategy(@Validated @RequestBody List<StrategyDto> strategyDtos){
        service.saveStrategy(strategyDtos);
    }

    @DeleteMapping(value="clearStrategy")
    public void clearStrategy(@NotBlank(message = "分类ID不能为空")String categoryId){
        service.clearStrategy(categoryId);
    }



    @GetMapping("queryStrategy")
    public  List<StrategyDto> queryStrategy(@NotBlank(message = "栏目ID不能为空") String categoryId){
        return service.queryStrategy(categoryId,null,null);
    }

}
