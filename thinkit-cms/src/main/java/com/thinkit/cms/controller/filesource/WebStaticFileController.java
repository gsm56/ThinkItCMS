package com.thinkit.cms.controller.filesource;

import com.thinkit.cms.api.filesource.WebStaticFileService;
import com.thinkit.cms.dto.template.TemplateContentDto;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.FileViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Validated
@RestController
@RequestMapping("webstatic")
public class WebStaticFileController {

    @Autowired
    WebStaticFileService wbStaticFileService;

    @Logs(module = LogModule.SITE_FILE,operation = "查看站点文件")
    @GetMapping("page")
    public List<FileViewModel> page(@RequestParam String path) {
        return wbStaticFileService.listPage(path);
    }


    @Logs(module = LogModule.SITE_FILE,operation = "删除站点文件")
    @DeleteMapping(value = "/deleteFile")
    public ApiResult deleteFile(@RequestParam("path") String filePath){
        return  wbStaticFileService.deleteFile(filePath);
    }


    @Logs(module = LogModule.SITE_FILE,operation = "创建站点目录")
    @PutMapping("mkdirFolder")
    public ApiResult createFile(@RequestParam(required = false,value = "filePath",defaultValue = "") String filePath,
                                @RequestParam(required = false,value = "folderName",defaultValue = "") String folderName){
        return wbStaticFileService.createFile(filePath,folderName);
    }

    @Logs(module = LogModule.SITE_FILE,operation = "压缩下载站点文件")
    @PostMapping("downZip")
    public void downZip(String path, HttpServletResponse response){
        wbStaticFileService.downZip(path,response);
    }


    @PostMapping("uploadFile")
    public ApiResult uploadFile(@RequestParam("files[]") List<MultipartFile> files, @RequestParam("filePath") String filePath){
        return wbStaticFileService.uploadFile(files,filePath);
    }


    @Logs(module = LogModule.SITE_FILE,operation = "查看站点文件内容")
    @GetMapping("getFileContent")
    public ApiResult getFileContent(@RequestParam String path) {
        return wbStaticFileService.getFileContent(path);
    }

    @PostMapping("setFileContent")
    public ApiResult setFileContent(@RequestBody TemplateContentDto templateContentDto){
        return wbStaticFileService.setFileContent(templateContentDto.getPath(),templateContentDto.getContent());
    }


}
