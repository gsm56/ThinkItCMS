package com.thinkit.cms.controller.admin;

import com.thinkit.cms.api.admin.MenuService;
import com.thinkit.cms.dto.admin.MenuDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.utils.model.Tree;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 菜单管理 前端控制器
 * </p>
 *
 * @author dl
 * @since 2018-03-21
 */
@RestController
@RequestMapping("/menu")
public class MenuController extends BaseController<MenuService> {

    @RequestMapping("/list")
    public List<MenuDto> selectList(MenuDto menu) {
        List<MenuDto> menus = service.listDto(menu);
        return menus;
    }

    @RequestMapping("info")
    public MenuDto info(@RequestParam String id) {
        return service.info(id);
    }

    @PostMapping("/save")
    public boolean save(@RequestBody MenuDto menuDto) {
        return service.save(menuDto);
    }

    @PostMapping("/update")
    public boolean update(@RequestBody MenuDto menuDto) {
        return service.update(menuDto);
    }

    @DeleteMapping("/delete")
    public boolean delete(String id) {
        return service.delete(id);
    }

    @RequestMapping("/selectTreeList")
    public Tree<MenuDto> selectTreeList() {
        Tree<MenuDto> menus = service.selectTreeList();
        return menus;
    }

    @Logs(module = LogModule.MENU,operation = "加载用户菜单")
    @GetMapping("loadMenu")
    public List<MenuDto> loadMenu() {
        return service.loadMenu(getUserId());
    }

    @Logs(module = LogModule.MENU,operation = "隐藏菜单")
    @PutMapping("hideIt")
    public void hideIt(@RequestParam String id,@RequestParam Integer hidden) {
         service.hideIt(id,hidden);
    }




    @Logs(module = LogModule.MENU,operation = "加载微服务实例")
    @GetMapping("loadApplications")
    public List<String> loadApplications() {
        return service.loadApplications();
    }


}
