package com.thinkit.cms.controller.content;

import com.thinkit.cms.api.content.ContentRelatedService;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.content.ContentRelatedDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 内容推荐 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-09-30
 */
@Validated
@RestController
@RequestMapping("contentRelated")
public class ContentRelatedController extends BaseController<ContentRelatedService> {


    @DeleteMapping("deleteRelated")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("saveRelatedBath")
    public  void saveRelatedBath(@RequestBody List<ContentRelatedDto> dtos){
         service.saveRelatedBath(dtos);
    }

    @GetMapping("listRelated")
    public  List<ContentDto> listRelated(@RequestParam String contentId){
        return service.listRelated(contentId);
    }

    @PostMapping("page")
    public PageDto<ContentRelatedDto> listPage(@RequestBody PageDto<ContentRelatedDto> pageDto){
        return service.listPage(pageDto);
    }

}
