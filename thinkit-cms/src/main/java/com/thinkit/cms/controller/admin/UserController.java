package com.thinkit.cms.controller.admin;

import com.thinkit.cms.api.admin.UserService;
import com.thinkit.cms.dto.admin.UserDto;
import com.thinkit.core.annotation.NotDecorate;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.log.enums.LogOperation;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.PageDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

@Validated
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController extends BaseController<UserService> {

    @Logs(module = LogModule.USER,operation = "进入/刷新首页")
    @GetMapping("info")
    public Map<String,Object> info() {
        return service.info();
    }


    @GetMapping("getById")
    public UserDto getById(@NotBlank(message = "id 不能为空") String id) {
        return service.getById(id);
    }

    @Logs(module =LogModule.USER,operation = "查询用户列表")
    @PostMapping("page")
    public PageDto<UserDto> page(@RequestBody PageDto<UserDto> userDto) {
        return service.listPage(userDto);
    }

    @Logs(module =LogModule.USER,operaEnum = LogOperation.SAVE)
    @PostMapping("save")
    public boolean save(@RequestBody UserDto userDto) {
        return service.save(userDto);
    }

    @Logs(module =LogModule.USER,operaEnum = LogOperation.UPDATE)
    @PostMapping("update")
    public boolean update(@Valid @RequestBody UserDto userDto) {
        return service.update(userDto);
    }

    @Logs(module =LogModule.USER,operaEnum = LogOperation.DELETE)
    @DeleteMapping("delete")
    public boolean delete(@NotBlank String id) {
        return service.deleteByUserId(id);
    }

    @Logs(module =LogModule.USER,operation = "批量操作用户")
    @PostMapping("batch")
    public boolean batch(@RequestBody UserDto userDto) {
        return service.batch(userDto.getType(), userDto.getUserIds());
    }

    @Logs(module =LogModule.USER,operation = "个人账户资料修改")
    @PutMapping("updateUserInfo")
    public boolean updateUserInfo(@RequestBody UserDto userDto) {
        return service.updateUserInfo(userDto);
    }

    @Logs(module = LogModule.USER,operation = "修改密码")
    @RequestMapping(value = "modifyPass", method= RequestMethod.PUT)
    public void modifyPass(@RequestBody UserDto user) {
        service.modifyPass(user);
    }

    @Logs(module =LogModule.USER,operation = "重置密码")
    @PutMapping("resetPass")
    public ApiResult resetPass(@NotBlank String id) {
        return service.resetPass(id);
    }

    @Logs(module =LogModule.USER,operation = "查找用户ID")
    @GetMapping("getUserIdsByOrgId")
    public List<String> getUserIdsByOrgId(@NotBlank @RequestParam("orgId") String orgId) {
        return service.getUserIdsByOrgId(orgId);
    }

    @GetMapping("verifyCode")
    public ApiResult verifyCode() {
        return service.verifyCode();
    }
}
