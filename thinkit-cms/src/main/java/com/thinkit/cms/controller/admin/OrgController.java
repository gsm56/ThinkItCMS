package com.thinkit.cms.controller.admin;

import com.thinkit.cms.api.admin.OrgService;
import com.thinkit.cms.api.admin.RoleService;
import com.thinkit.cms.api.admin.UserRoleService;
import com.thinkit.cms.dto.admin.OrgDto;
import com.thinkit.core.annotation.NotDecorate;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.log.enums.LogOperation;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.model.Tree;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;

@Validated
@RequestMapping(value={"org"})
@RestController
public class OrgController extends BaseController<OrgService> {

	@Resource
	private RoleService roleService;
	
	@Resource
	private UserRoleService userRoleService;
	
	@Resource
	private BCryptPasswordEncoder bCryptPasswordEncoder;


	@Logs(module = LogModule.ORG,operaEnum = LogOperation.VIEW)
	@RequestMapping(value = "/page", method = RequestMethod.POST)
	public PageDto<OrgDto> list(@RequestBody PageDto<OrgDto> pageDto) {
		return service.listPage(pageDto);
	}


	@Logs(module = LogModule.ORG,operaEnum = LogOperation.SAVE)
	@RequestMapping(value="save",method= RequestMethod.POST)
	public void save(@Validated @RequestBody OrgDto orgDto) {
		service.saveOrg(orgDto);
    }

	@RequestMapping(value="info",method = RequestMethod.GET)
	public OrgDto info(@NotBlank @RequestParam String id){
		return service.info(id);
	}

	@RequestMapping("/selectTreeList")
	public Tree<OrgDto> selectTreeList(){
		Tree<OrgDto> menus=service.selectTreeList();
		return menus;
	}

	@NotDecorate
	@RequestMapping("/getOrgTres")
	public Tree<OrgDto> getOrgTres(){
		return service.selectTreeList();
	}

	@Logs(module = LogModule.ORG,operaEnum = LogOperation.UPDATE)
	@RequestMapping(value="update",method= RequestMethod.PUT)
	public void update(@Validated @RequestBody OrgDto orgDto) {
		service.update(orgDto);
	}


	@Logs(module = LogModule.ORG,operaEnum = LogOperation.DELETE)
	@DeleteMapping(value="delete")
	public boolean delByPk(@NotBlank @RequestParam String id){
		return service.deleteOrg(id);
	}

}
