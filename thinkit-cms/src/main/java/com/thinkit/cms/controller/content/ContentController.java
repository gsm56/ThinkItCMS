package com.thinkit.cms.controller.content;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.dto.content.ContentAnalysisDto;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.content.PublishDto;
import com.thinkit.core.annotation.SiteMark;
import com.thinkit.core.annotation.valid.ValidGroup1;
import com.thinkit.core.annotation.valid.ValidGroup2;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.utils.enums.SiteOperation;
import com.thinkit.utils.enums.StatusEnum;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.DynamicModel;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.model.Tree;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 内容 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Validated
@RestController
@RequestMapping("content")
public class ContentController extends BaseController<ContentService> {

    @GetMapping("getDetail")
    public Map<String,Object> getDetail(@NotBlank @RequestParam String id){
       return  service.getDetail(id);
    }


    @Logs(module = LogModule.CONTENT,operation = "公众号或者头条同步")
    @GetMapping("wechatArticle")
    public Map<String,Object> wechatArticle(@NotBlank @RequestParam String url,
                                            @NotBlank @RequestParam String modelId,
                                            @NotNull @RequestParam Integer type,
                                            @RequestParam(required =  false,defaultValue = "") String cookie){
        return  service.wechatArticle(url,modelId,type,cookie);
    }

    @Logs(module = LogModule.CONTENT,operation = "创建文章内容")
    @SiteMark
    @PostMapping(value="save")
    public void save(@Validated(value = {ValidGroup1.class}) @RequestBody ContentDto v){
        service.save(v);
    }


    @Logs(module = LogModule.CONTENT,operation = "编辑文章内容")
    @SiteMark
    @PutMapping("update")
    public void update(@Validated(value = {ValidGroup1.class}) @RequestBody ContentDto v){
        service.update(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.delete(id,false);
    }



    @DeleteMapping(value = "delRealByPks")
    public void delRealByPks(@NotEmpty @RequestBody List<String> ids){
        service.deletes(ids,true);
    }


    @PutMapping(value = "recyclerByPks")
    public void recyclerByPks(@NotEmpty @RequestBody List<String> ids){
        service.recyclerByPks(ids);
    }


    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deletes(ids,false);
    }

    @Logs(module = LogModule.CONTENT,operation = "文章发布或者取消发布")
    @PutMapping(value = "publish")
    public void publish(@Validated @RequestBody PublishDto publishDto){
        service.publish(publishDto,false);
    }

    @Logs(module = LogModule.CONTENT,operation = "重新批量生成静态页")
    @PostMapping(value="reStaticBatchGenCid")
    public void reStaticFileByCid(@NotEmpty @RequestBody List<String> ids){
        service.reStaticBatchGenCid(ids);
    }

    // 内容发布管理用
    @GetMapping("/treeCategory")
    public Tree<CategoryDto> treeCategory() {
        Tree<CategoryDto> treeCategorys = service.treeCategory();
        return treeCategorys;
    }

    // 创建文章是 构造表单
    @GetMapping("/getFormDesign")
    public List<DynamicModel> getFormDesign(@NotBlank @RequestParam String modelId) {
        return service.getFormDesign(modelId);
    }


    @SiteMark(type = SiteOperation.READ)
    @PostMapping("page")
    public PageDto<ContentDto> listPage(@RequestBody PageDto<ContentDto> pageDto){
        return service.listPage(pageDto);
    }


    @SiteMark(type = SiteOperation.READ)
    @PostMapping("pageRecycler")
    public PageDto<ContentDto> pageRecycler(@RequestBody PageDto<ContentDto> pageDto){
        pageDto.getDto().setStatus(StatusEnum.DELETE.getCode());
        return service.pageRecycler(pageDto);
    }



    @Logs(module = LogModule.CONTENT,operation = "置顶内容")
    @PutMapping("top")
    public void top(@Validated(value = {ValidGroup2.class}) @RequestBody ContentDto contentDto){
          service.top(contentDto);
    }

    @Logs(module = LogModule.CONTENT,operation = "查询置顶标签")
    @GetMapping("getTopTag")
    public Set<String> getTopTag(){
        Set<String> topTags=  service.getTopTag();
        return topTags;
    }


    @GetMapping("viewContent")
    public ApiResult viewContent(String id){
        return service.viewContent(id);
    }


    @Logs(module = LogModule.CONTENT,operation = "移动内容到指定栏目的")
    @PutMapping("move")
    public ApiResult move(@RequestBody ContentDto contentDto){
        return  service.move(contentDto.getCategoryId(),contentDto.getContentIds());
    }

    @SiteMark
    @Logs(module = LogModule.CONTENT,operation = "统计文章月份发布量")
    @GetMapping("analysisMonthTop")
    public Map<String,Object> analysisMonthTop(ContentAnalysisDto contentAnalysisDto){
        return service.analysisMonthTop(contentAnalysisDto);
    }
}
