package com.thinkit.cms.entity.fragment;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 页面片段数据表
 * </p>
 *
 * @author lg
 * @since 2020-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_fragment_attr")
public class FragmentAttr extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 封面图
     */

    @TableField("cover")
    private String cover;


    @NotBlank(message = "站点ID不能为空")
    @TableField("site_id")
    private String siteId;


    @TableField(exist = false)
    private String siteName;


    /**
     * 片段路径
     */

    @TableField("path")
    private String path;


    /**
     * 标题
     */

    @TableField("title")
    private String title;


    @TableField("description")
    private String description;


    /**
     * 超链接
     */

    @TableField("url")
    private String url;


    /**
     * 排序
     */

    @TableField("sort")
    private Integer sort;


    /**
     * 扩展数据data
     */

    @TableField("data")
    private String data;


    /**
     * 模板id
     */

    @TableField("template_id")
    private String templateId;


    @TableField("fragment_model_id")
    private String fragmentModelId;


    /**
     * 0:未激活 1：已激活
     */

    @TableField("activate")
    private Boolean activate;

}
