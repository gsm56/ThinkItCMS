package com.thinkit.cms.entity.category;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * <p>
 * 分类
 * </p>
 *
 * @author lg
 * @since 2020-08-07
 */
@Data
@Accessors(chain = true)
@TableName("tk_category")
public class Category extends BaseModel {

private static final long serialVersionUID = 1L;

    /**
     * 名称
     */

        @TableField("name")
        private String name;


    /**
     * 父分类ID
     */

        @TableField("parent_id")
        private String parentId;


    /**
     * 站点ID
     */

        @TableField("site_id")
        private String siteId;


    /**
     * 编码
     */

        @TableField("code")
        private String code;


    /**
     * 生成路径规则
     */

        @TableField("path_rule")
        private String pathRule;


    /**
     * 分类首页路径
     */

        @TableField("path")
        private String path;


    /**
     * 是否只外链 只是外链栏目直接访问
     */

        @TableField("only_url")
        private Boolean onlyUrl;


    /**
     * 外链跳转地址
     */

        @TableField("remote_url")
        private String remoteUrl;


    /**
     * 每页数据条数
     */

        @TableField("page_size")
        private Integer pageSize;


    /**
     * 允许投稿
     */

        @TableField("allow_contribute")
        private Boolean allowContribute;


    /**
     * 顺序
     */

        @TableField("sort")
        private Integer sort;


    /**
     * 是否在首页隐藏
     */

        @TableField("hidden")
        private Boolean hidden;


    /**
     * 扩展ID
     */
        @TableField("category_model_id")
        private String categoryModelId;


    /**
     * 是否是单页
     */

        @TableField("single_page")
        private Boolean singlePage;


    /**
     * 该栏目多少秒后自动排序生成
     */

        @TableField("top_pages")
        private Integer topPages;


    /**
     * 是否包含字内容
     */

        @TableField("contain_child")
        private Boolean containChild;

}
