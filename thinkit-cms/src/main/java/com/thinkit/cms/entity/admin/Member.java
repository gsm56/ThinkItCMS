package com.thinkit.cms.entity.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lg
 * @since 2020-11-03
 */
@Data
@Accessors(chain = true)
@TableName("tk_member")
public class Member extends BaseModel {

private static final long serialVersionUID = 1L;

    /**
     * 真实姓名
     */

        @TableField("real_name")
        private String realName;


    /**
     * 会员昵称
     */

        @TableField("nick_name")
        private String nickName;


    /**
     * 头像
     */

        @TableField("head_img")
        private String headImg;


    /**
     * 账号
     */

        @TableField("account")
        private String account;


    /**
     * 密码
     */

        @TableField("password")
        private String password;


    /**
     * 手机号
     */

        @TableField("tel")
        private String tel;


    /**
     * 邮箱
     */

        @TableField("email")
        private String email;


    /**
     * 性别
     */

        @TableField("sex")
        private Integer sex;


    /**
     * 注册时间
     */

        @TableField("register_date")
        private LocalDateTime registerDate;


    /**
     * 是否锁定 0：未锁定 1：已锁定
     */

        @TableField("is_lock")
        private Integer isLock;


    /**
     * 0:未删除 1：已删除
     */

        @TableField("is_del")
        private Integer isDel;


    /**
     * 创建用户id
     */



    /**
     * 修改人id
     */



    /**
     * 创建时间
     */



    /**
     * 修改时间
     */






}
