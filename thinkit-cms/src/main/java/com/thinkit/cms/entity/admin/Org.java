package com.thinkit.cms.entity.admin;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("sys_org")
public class Org  extends BaseModel {

    @TableField("org_name")
    private String orgName;
    /**
     * 部门编码
     */
    @TableField("org_code")
    private String orgCode;
    /**
     * 父级部门id
     */
    @TableField("parent_id")
    private String parentId;


    @TableField("level")
    private String level;
    /**
     * 父级部门编码
     */
    @TableField("parent_code")
    private String parentCode;
    /**
     * 备注
     */
    @TableField("comment")
    private String comment;

}

