package com.thinkit.cms.entity.site;

import java.io.Serializable;

import com.thinkit.utils.model.BaseModel;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * <p>
 * 站点管理表
 * </p>
 *
 * @author LG
 * @since 2020-07-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_site")
public class Site extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 站点名称
     */

    @TableField("site_name")
    private String siteName;


    /**
     * 站点副标题
     */

    @TableField("site_subtitle")
    private String siteSubtitle;

    @TableField("is_default")
    private Boolean isDefault;


    /**
     * 组织内共享
     */
    @TableField("is_share_org")
    private Boolean isShareOrg;

    /**
     * 关键字
     */

    @TableField("site_key_words")
    private String siteKeyWords;


    /**
     * 站点描述
     */

    @TableField("site_desc")
    private String siteDesc;


    /**
     * 站点logo
     */

    @TableField("site_logo")
    private String siteLogo;


    /**
     * 域名
     */

    @TableField("domain")
    private String domain;

    /**
     * 组织ID
     */

    @TableField("org_id")
    private String orgId;



    /**
     * 站点备案信息
     */

    @TableField("siteicp")
    private String siteicp;


    /**
     * 站点版权
     */

    @TableField("copyright")
    private String copyright;


    /**
     * 第三方评论代码
     */

    @TableField("comment_code")
    private String commentCode;


    /**
     * 第三方分享代码
     */

    @TableField("share_code")
    private String shareCode;


    /**
     * 第三方统计代码
     */

    @TableField("statistical_code")
    private String statisticalCode;


    /**
     * 创建用户id
     */


    /**
     * 修改人id
     */


    /**
     * 创建时间
     */


    /**
     * 修改时间
     */


}
