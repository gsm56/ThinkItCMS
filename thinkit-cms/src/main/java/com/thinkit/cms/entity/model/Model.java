package com.thinkit.cms.entity.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sun.org.apache.xpath.internal.operations.Bool;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author LG
 * @since 2019-10-23
 */
@Data
@Accessors(chain = true)
@TableName("tk_model")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Model extends BaseModel {

    private static final long serialVersionUID = 1L;
    /**
     * 模型名称
     */

    @TableField("name")
    private String name;

    /**
     * 模型名称
     */

    @TableField(exist = false)
    private Boolean check = false;


    @TableField(exist = false)
    private String templatePath ;

    @TableField(exist = false)
    private String md5Key;


    @TableField("site_id")
    private String siteId;


    /**
     * 是否有用图片
     */

    @TableField("has_images")
    private Boolean hasImages;


    /**
     * 是否拥有文件
     */

    @TableField("has_files")
    private Boolean hasFiles;


    @TableField("is_url")
    private Boolean isUrl;


    @TableField("support_wehcat")
    private Boolean supportWehcat;

    @TableField("support_tops")
    private Boolean supportTops;




    /**
     * 全部选中字段
     */
    @TableField("all_field_list")
    private String allFieldList;


    /**
     * 选中字段
     */

    @TableField("default_field_list")
    private String defaultFieldList;


    /**
     * 扩展字段
     */

    @TableField("extend_field_list")
    private String extendFieldList;


    /**
     * 必填字段
     */

    @TableField("required_field_list")
    private String requiredFieldList;


    /**
     * 字段对应中文名称
     */

    @TableField("field_text_map")
    private String fieldTextMap;


}
