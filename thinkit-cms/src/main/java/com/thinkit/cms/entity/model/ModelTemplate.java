package com.thinkit.cms.entity.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;
/**
 * <p>
 * 模型模板针对不同的站点不同的模型下有不同的 模板页面
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Data
@Accessors(chain = true)
@TableName("tk_model_template")
public class ModelTemplate extends BaseModel {

private static final long serialVersionUID = 1L;

    /**
     * 模型ID
     */

        @TableField("model_id")
        private String modelId;


    /**
     * 站点ID
     */

        @TableField("site_id")
        private String siteId;


    /**
     * 模板ID
     */

        @TableField("template_id")
        private String templateId;


    /**
     * 内容静态化模板地址
     */

        @TableField("template_path")
        private String templatePath;





}
