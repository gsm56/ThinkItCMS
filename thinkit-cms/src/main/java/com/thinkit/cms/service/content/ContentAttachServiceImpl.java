package com.thinkit.cms.service.content;

import com.thinkit.cms.api.content.ContentAttachService;
import com.thinkit.cms.dto.content.ContentAttachDto;
import com.thinkit.cms.entity.content.ContentAttach;
import com.thinkit.cms.mapper.content.ContentAttachMapper;
import com.thinkit.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 内容附件 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-08-19
 */
@Transactional
@Service
public class ContentAttachServiceImpl extends BaseServiceImpl<ContentAttachDto, ContentAttach, ContentAttachMapper> implements ContentAttachService {


    @Override
    public List<String> listData(String contentId) {
        return baseMapper.listData(contentId);
    }

    @Override
    public List<Map> listAttach(String contentId) {
        return baseMapper.listAttach(contentId);
    }

    @Override
    public List<Map> listAttachs(List<String> contentIds) {
        return baseMapper.listAttachs(contentIds);
    }
}
