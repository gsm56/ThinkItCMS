package com.thinkit.cms.service.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.admin.RoleMenuService;
import com.thinkit.cms.api.admin.RoleService;
import com.thinkit.cms.api.admin.UserRoleService;
import com.thinkit.cms.dto.admin.RoleDto;
import com.thinkit.cms.entity.admin.Role;
import com.thinkit.cms.mapper.admin.RoleMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.nosql.annotation.CacheClear;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author dl
 * @since 2018-03-19
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<RoleDto, Role, RoleMapper> implements RoleService {


    @Autowired
    UserRoleService userRoleService;

    @Autowired
    RoleMenuService roleMenuService;

    @CacheClear(clas = {MenuServiceImpl.class},method = "loadMenu",key = "#clas.targetClass+'.'+#clas.method+'.*'")
    @Override
    public ApiResult deleteById(String roleId) {
        if (roleId.equals("1")) {
            return ApiResult.result(5001);
        }
        List<String> userIds = userRoleService.selectUserIdByRId(roleId);
        if (Checker.BeNotEmpty(userIds)) {
            return ApiResult.result(5002);
        } else {
            super.removeById(roleId);
            return ApiResult.result();
        }
    }

    @Override
    public Set<String> selectRoleSignByUid(String userId) {
        Set<String> roleSigns = new HashSet<>();
        List<String> roleIds = userRoleService.selectRoleIdByUId(userId);
        List<Role> roles = listByIds(roleIds);
        for (Role role : roles) {
            if (Checker.BeNotBlank(role.getRoleSign())) {
                roleSigns.add(role.getRoleSign());
            }
        }
        return roleSigns;
    }

    @Override
    public List<RoleDto> selectAllRoles() {
        QueryWrapper<Role> wrapper = new QueryWrapper<Role>();
        //wrapper.notIn("role_sign", "administrator");
        List<Role> roles = list(wrapper);
        return Checker.BeNotEmpty(roles) ? T2DList(roles) : Lists.newArrayList();
    }

}
