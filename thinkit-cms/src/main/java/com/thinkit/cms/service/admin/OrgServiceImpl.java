package com.thinkit.cms.service.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.admin.OrgService;
import com.thinkit.cms.api.admin.UserRoleService;
import com.thinkit.cms.api.admin.UserService;
import com.thinkit.cms.dto.admin.OrgDto;
import com.thinkit.cms.dto.admin.UserDto;
import com.thinkit.cms.entity.admin.Org;
import com.thinkit.cms.mapper.admin.OrgMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.core.handler.CustomException;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.Tree;
import com.thinkit.utils.utils.BuildTree;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrgServiceImpl extends BaseServiceImpl<OrgDto, Org, com.thinkit.cms.mapper.admin.OrgMapper> implements OrgService {
	
	@Autowired
	private UserService userService;
	
	@Resource
	private OrgMapper OrgMapper;
	
	@Resource
	private UserRoleService userRoleService;


	@Override
	public Tree<OrgDto> selectTreeList() {
		List<OrgDto> orgs=super.listDto(new OrgDto());
		if(Checker.BeNotEmpty(orgs)){
			List<Tree<OrgDto>> trees = new ArrayList<Tree<OrgDto>>();
			for (OrgDto orgDto : orgs) {
				Tree<OrgDto> tree = new Tree<OrgDto>();
				tree.setId(orgDto.getId());
				tree.setKey(orgDto.getId());
				tree.setValue(orgDto.getId());
				tree.setParentId(orgDto.getParentId());
				tree.setName(orgDto.getOrgName());
				tree.setTitle(orgDto.getOrgName());
				Map<String, Object> attributes = new HashMap<>(16);
				attributes.put("orgCode", orgDto.getOrgCode());
				attributes.put("parentCode", orgDto.getParentCode());
				attributes.put("gmt_create", orgDto.getGmtCreate());
				attributes.put("level",orgDto.getLevel());
				tree.setAttributes(attributes);
				trees.add(tree);
			}
			Tree<OrgDto> t = BuildTree.build(trees);
			return t;
		}else{
			return null;
		}
	}

	@Override
	public void saveOrg(OrgDto orgDto) {
		if(ckLegal(orgDto.getOrgCode(),null)){
			throw new CustomException(ApiResult.result(5015));
		};
		orgDto.setId(id());
		OrgDto parentOrg=getByPk(orgDto.getParentId());
		if(Checker.BeNotNull(parentOrg)){
			orgDto.setParentCode(parentOrg.getOrgCode());
		}
		insert(orgDto);
	}

	@Override
	public void update(OrgDto orgDto) {
		if(ckLegal(orgDto.getOrgCode(),orgDto.getId())){
			throw new CustomException(ApiResult.result(5015));
		}
		super.updateByPk(orgDto);
	}

	private boolean ckLegal(String code,String id){
		Integer count = baseMapper.ckCode(code,id);
		return count>0;
	}

	@Override
	public boolean deleteOrg(String id) {
        List<String> ids=new ArrayList<>();
        List<String> total=new ArrayList<>();
        ids.add(id);
        total.add(id);
        getOrgIds(ids,total);
        if(Checker.BeNotEmpty(total)){
            for(String oid: total){
                List<UserDto> userDtos=userService.getUserByOrgId(oid);
                if(Checker.BeNotEmpty(userDtos)){
                    throw new CustomException(ApiResult.result(5004));
                }
            }
           deleteByPks(total);
        }
        return true;
	}

	private List<String> getOrgIds(List<String>ids,List<String> total){
	    if(Checker.BeNotEmpty(ids)){
	        for(String id:ids){
                OrgDto orgDto=new OrgDto();
				orgDto.setParentId(id);
                List<OrgDto> list=listDto(orgDto);
                if(Checker.BeNotEmpty(list)){
                   List<String> newlist=new ArrayList<>();
                   for(OrgDto org:list){
                       if(!newlist.contains(org.getId()))
                       newlist.add(org.getId());
                   }
                   total.addAll(newlist);
                   getOrgIds(newlist,total);
                }
            }
        }
        return total;
    }

    @Override
    public OrgDto info(String id) {
		OrgDto childOrgDto=getByPk(id);
		OrgDto parentOrgDto=getByPk(childOrgDto.getParentId());
        if(Checker.BeNotNull(parentOrgDto)){
            childOrgDto.setParentName(parentOrgDto.getOrgName());
        }else{
            childOrgDto.setParentName("");
        }
        return childOrgDto;
    }

	@Override
	public void deleteByOrgCode(String code) {
		baseMapper.deleteByOrgCode(code);
	}

	@Override
	public List<OrgDto> listByParentId(String parentId) {
		QueryWrapper<Org> wrapper = new QueryWrapper<>();
		wrapper.eq(Checker.BeNotNull(parentId), "parent_id", parentId);
		List<Org> orgList = OrgMapper.selectList(wrapper);
		return Checker.BeNotNull(orgList) ? T2DList(orgList) : Lists.newArrayList();
	}


	@Override
	public List<OrgDto> getOrgsByUserId(String userId, String orgCode) {
		UserDto userDto=userService.getByPk(userId);
		String goalOrgCode=null;//userDto.getOrgCode().split("-")[0]+orgCode;
		return baseMapper.getOrgsByUserId(goalOrgCode);
	}



}
