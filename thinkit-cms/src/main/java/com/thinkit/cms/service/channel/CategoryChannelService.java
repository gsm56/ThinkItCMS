package com.thinkit.cms.service.channel;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.core.constant.Channel;
import com.thinkit.processor.channel.BaseChannelAdaptService;
import com.thinkit.processor.message.MessageSend;
import com.thinkit.utils.enums.ChannelEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CategoryChannelService extends BaseChannelAdaptService implements Cloneable  {

    @Autowired
    CategoryService categoryService;

    private String categoryId;

    private Boolean notify = false;

    public void notifyIt(String categoryId){
        this.categoryId = categoryId;
        execuate(true);
    }

    public void notifyIt(String categoryId,Boolean notify){
        this.categoryId = categoryId;
        if(notify){
            this.notify = true;
            execuate(true,true);
        }else{
            execuate(true);
        }
    }

    public void callBack(Object param){

    }

    @Override
    protected String loadTempPath() {
        return super.tempPath;
    }

    @Override
    protected String loadDestPath() {
        return super.destPath;
    }

    @Override
    protected Map<String, Object> loadTempParams(Map<String,Object> variable) {
        Map<String,Object> params = categoryService.loadTempParams(categoryId);
        setPath(params,false);
        return params;
    }


    @Override
    public ChannelEnum getName() {
        return ChannelEnum.CATEGORY;
    }
}
