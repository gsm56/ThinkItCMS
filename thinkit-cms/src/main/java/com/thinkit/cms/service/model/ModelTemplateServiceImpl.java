package com.thinkit.cms.service.model;

import com.thinkit.cms.api.model.ModelTemplateService;
import com.thinkit.cms.dto.model.ModelTemplateDto;
import com.thinkit.cms.entity.model.ModelTemplate;
import com.thinkit.cms.mapper.model.ModelTemplateMapper;
import com.thinkit.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 模型模板针对不同的站点不同的模型下有不同的 模板页面 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Transactional
@Service
public class ModelTemplateServiceImpl extends BaseServiceImpl<ModelTemplateDto, ModelTemplate, ModelTemplateMapper> implements ModelTemplateService {


    @Override
    public void saveModelTemp(String modelId, String siteId, String templateId, String templatePath) {
        ModelTemplateDto modelTemplateDto =new ModelTemplateDto();
        modelTemplateDto.setModelId(modelId).setSiteId(siteId).setTemplateId(templateId).setTemplatePath(templatePath);
        super.insert(modelTemplateDto);
    }

    @Override
    public ModelTemplateDto getModelTemplate(String modelId, String templateId ,String siteId) {
        return baseMapper.getModelTemplate(modelId,templateId,siteId);
    }
}
