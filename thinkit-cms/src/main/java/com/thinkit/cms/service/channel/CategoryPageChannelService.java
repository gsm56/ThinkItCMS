package com.thinkit.cms.service.channel;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.processor.channel.BaseChannelAdaptService;
import com.thinkit.utils.enums.ChannelEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CategoryPageChannelService extends BaseChannelAdaptService implements Cloneable  {

    @Autowired
    ContentService contentService;

    private Map<String,Object> params;

    private Boolean notify =false;

    public void notifyIt(Map<String,Object> map){
        this.params = map;
        execuate(true);
    }

    public void notifyIt(Map<String,Object> map,Boolean notify){
        this.params = map;
        if(notify){
            this.notify = true;
            execuate(true,true);
        }else{
            execuate(true);
        }
    }

    public void callBack(Object o){
        System.out.println(o);
    }

    @Override
    protected String loadTempPath() {
        return super.tempPath;
    }

    @Override
    protected String loadDestPath() {
        return super.destPath;
    }

    @Override
    protected Map<String, Object> loadTempParams(Map<String,Object> variable) {
        Map<String,Object> params = contentService.listContent(this.params,notify);
        setPath(params,false);
        return params;
    }

    @Override
    public ChannelEnum getName() {
        return ChannelEnum.CATEGORY_PAGE;
    }
}
