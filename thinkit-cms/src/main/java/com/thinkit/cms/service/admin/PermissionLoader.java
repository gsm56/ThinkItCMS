package com.thinkit.cms.service.admin;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.thinkit.cms.dto.admin.Permission;
import com.thinkit.cms.mapper.admin.PermissionMapper;
import com.thinkit.core.constant.Constants;
import com.thinkit.core.constant.SecurityConstants;
import com.thinkit.nosql.base.BaseRedisService;
import com.thinkit.utils.enums.UserFrom;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PermissionLoader {

    @Resource
    private PermissionMapper permissionMapper;

    @Autowired
    BaseRedisService baseRedisService;

    protected Set<String> loadPermsByUid(String userId, String clientId){
        Set<String>  perms;
        if(UserFrom.ckIsPlatUser(UserFrom.getUserFrom(clientId))){
            perms=loadPlatPermsByUid(userId);
            return perms;
        }else{
            // urls=permissionMapper.selectPlatPermIdsAndUrl(application);
            // return filterBuild(urls);
            return null;
        }
    }

    private Set<String> loadPlatPermsByUid(String userId) {
        Set<String> permSet;
        String key= SecurityConstants.PERMISSION_ALL+userId;
        if(hasKey(key)){
            permSet = getKeyAsSet(key);
        }else{
            permSet = permissionMapper.selectPlatPermIdsByUid(userId);
            setKey(key,permSet);
        }
        return  Checker.BeNotEmpty(permSet)?permSet: Sets.newHashSet();
    }


    /**
     * 根据url
     * @param clientId
     * @return
     */
    protected Map<String,String> loadPermsByUrl(String clientId){
      List<Permission> urls;
      if(UserFrom.ckIsPlatUser(UserFrom.getUserFrom(clientId))){
          urls=loadPlatUrls(clientId);
          return filterBuild(urls);
      }else{
          //TODO 查询会员端的接口标识
         // urls=permissionMapper.selectPlatPermIdsAndUrl(application);
         // return filterBuild(urls);
          return null;
      }
    }

    private Map<String,String> filterBuild(List<Permission>  permUrls){
        Map<String,String> urlMap = new HashMap<>();
        if(Checker.BeNotEmpty(permUrls)){
            for (Permission perm :permUrls){
                String permUrl = perm.getUrl();
                if(Checker.BeNotBlank(permUrl)){
                    String[] urls=  permUrl.split(",");
                    if(urls.length>0){
                        for(String url:urls){
                            urlMap.put(url,perm.getPerms());
                        }
                    }
                }
            }
        }
        return  urlMap;
    }

    private List<Permission> loadPlatUrls(String clientId){
        String key= SecurityConstants.PERMISSION+clientId;
        List<Permission> urls ;
        if(hasKey(key)){
            urls = getKeyAsList(key);
        }else{
            urls=permissionMapper.selectPlatPermIdsAndUrl();
            setKey(key,urls);
        }
        return Checker.BeNotEmpty(urls)?urls: Lists.newArrayList();
    }

    protected String getPerm(Map<String,String> map,String url){
        if(!map.isEmpty()){
            return map.get(url);
        }
        return null;
    }


    private boolean hasKey(String key){
        return baseRedisService.hasKey(key);
    }

    private void setKey(String key,Object object){
         baseRedisService.set(key,object);
    }

    private  List<Permission> getKeyAsList(String key){
      return  (List<Permission>) baseRedisService.get(key);
    }

    private  Set<String> getKeyAsSet(String key){
        return  ( Set<String>) baseRedisService.get(key);
    }

}
