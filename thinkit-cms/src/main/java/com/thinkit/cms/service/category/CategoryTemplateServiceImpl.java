package com.thinkit.cms.service.category;

import com.thinkit.cms.api.category.CategoryTemplateService;
import com.thinkit.cms.dto.category.CategoryTemplateDto;
import com.thinkit.cms.entity.category.CategoryTemplate;
import com.thinkit.cms.mapper.category.CategoryTemplateMapper;
import com.thinkit.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 分类-模板配置表 服务实现类
 * </p>
 *
 * @author LG
 * @since 2020-08-12
 */
@Transactional
@Service
public class CategoryTemplateServiceImpl extends BaseServiceImpl<CategoryTemplateDto, CategoryTemplate, CategoryTemplateMapper> implements CategoryTemplateService {


    @Override
    public void updateTemplate(String categoryId, String siteId, String templateId, String templatePath) {
        baseMapper.updateTemplate(categoryId,siteId,templateId,templatePath);
    }

    @Override
    public String getCategoryTempPath(String categoryId, String siteId, String templateId) {
        return  baseMapper.getCategoryTempPath(categoryId,siteId,templateId);
    }

    @Override
    public CategoryTemplateDto getCategoryTemp(String categoryId, String siteId, String templateId) {
        return  baseMapper.getCategoryTemp(categoryId,siteId,templateId);
    }

    @Override
    public String getTempPathByCategoryId(String categoryId,String siteId) {
        return baseMapper.getTempPathByCategoryId(categoryId,siteId);
    }
}
