package com.thinkit.cms.mapper.content;
import com.thinkit.cms.entity.content.ContentAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 内容扩展 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Mapper
public interface ContentAttrMapper extends BaseMapper<ContentAttr> {

}
