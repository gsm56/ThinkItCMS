package com.thinkit.cms.strategy.actuator;

import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.entity.content.Content;
import com.thinkit.cms.strategy.checker.PublishChecker;
import com.thinkit.cms.strategy.convers.SolrConvert;
import com.thinkit.cms.strategy.filter.FilterChain;
import com.thinkit.cms.strategy.filter.TmpActuator;
import com.thinkit.core.annotation.Actuator;
import com.thinkit.nosql.base.BaseSolrService;
import com.thinkit.nosql.enums.SolrCoreEnum;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.utils.Checker;
import com.thinkit.utils.utils.SpringContextHolder;
import lombok.Getter;
import lombok.Setter;
import org.apache.solr.common.SolrDocument;
import org.quartz.JobExecutionContext;

import java.util.*;

@Actuator(code ="SOLR_STORE",name = "SOLR内容存储执行器")
public class SolrStoreActuator extends TmpActuator {

    @Getter @Setter
    private ActionEnum actionEnum;

    private BaseSolrService solrService;

    private SolrConvert solrConvert;

    @Getter
    @Setter
    private Object params;

    public SolrStoreActuator(){
        super();
    }

    public SolrStoreActuator(Object params,ActionEnum actionEnum){
        super();
        this.params = params;
        this.actionEnum =actionEnum;
        this.solrService = SpringContextHolder.getBean(BaseSolrService.class);
        this.solrConvert = SpringContextHolder.getBean(SolrConvert.class);
    }

    @Override
    public void doFilter(Object params, FilterChain chain) {
        if(isNotJob()) {
            Object content =solrConvert.convertParam(params,actionEnum,this);
            if(Checker.BeNotNull(content) && content instanceof ContentDto){
                ContentDto contentDto =  (ContentDto)content;
                if(ActionEnum.CREATE_CONTENT.equals(actionEnum)){
                    solrService.addContent(SolrCoreEnum.DEFAULT_CORE,content);
                }else if(ActionEnum.UPDATE_CONTENT.equals(actionEnum)){
                    String cid = contentDto.getId();
                    SolrDocument solrDocument =solrService.getById(SolrCoreEnum.DEFAULT_CORE,cid);
                    if(Checker.BeNull(solrDocument)){
                        List<ContentDto> contents = contentService.listContents(Arrays.asList(cid),contentDto.getStatus());
                        solrService.addContents(SolrCoreEnum.DEFAULT_CORE,contents);
                    }else{
                        solrService.updateFieldById(SolrCoreEnum.DEFAULT_CORE,cid,content);
                    }
                }
            }else if(Checker.BeNotNull(content) && content instanceof List){
                if(ActionEnum.PUBLISH_CONTENT.equals(actionEnum)){
                    List<String> cids = (List) content;
                    PublishChecker checker = (PublishChecker)this.getChecker();
                    // 判断 solr 库是否存在文章 如果存在则更新，不存在则添加
                    if(Checker.BeNotEmpty(cids)){
                        List<String> noStoreIds = new ArrayList<>();
                        List<SolrDocument> documents = new ArrayList<>();
                        for(String cid:cids){
                            SolrDocument solrDocument =  solrService.getById(SolrCoreEnum.DEFAULT_CORE,cid);
                            if(Checker.BeNotNull(solrDocument)){
                                solrDocument.setField("status",checker.getStatus());
                                if("1".equals(checker.getStatus())){
                                    solrDocument.setField("publishDate",new Date());
                                }
                                documents.add(solrDocument);
                            }else{
                                noStoreIds.add(cid);
                            }
                        }
                        solrService.updateDocs(SolrCoreEnum.DEFAULT_CORE,documents);
                        if(Checker.BeNotEmpty(noStoreIds)){
                            List<ContentDto> contents = contentService.listContents(noStoreIds,checker.getStatus());
                            solrService.addContents(SolrCoreEnum.DEFAULT_CORE,contents);
                        }
                    }
                }
            }
        }else{
            chain.addJobFilter(this);
        }
        chain.doFilter(params,chain);
    }

    private List<String> cids (List<ContentDto> contents){
        List<String> cids= new ArrayList<>();
        if(Checker.BeNotEmpty(contents)){
            for(ContentDto content:contents){
                cids.add(content.getId());
            }
        }
        return cids;
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext)  {
        Map dateMap = jobExecutionContext.getJobDetail().getJobDataMap();

    }

}
