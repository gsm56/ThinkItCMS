package com.thinkit.cms.strategy.checker;

import com.thinkit.utils.enums.ActionEnum;

public interface ActuatorCheck {

    boolean executeFilter(Object arg);


    void setAction(ActionEnum actionEnum);

    Object getParam();
}
