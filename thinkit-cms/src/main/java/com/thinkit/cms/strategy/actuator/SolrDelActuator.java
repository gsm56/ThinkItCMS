package com.thinkit.cms.strategy.actuator;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.strategy.convers.SolrConvert;
import com.thinkit.cms.strategy.filter.FilterChain;
import com.thinkit.cms.strategy.filter.TmpActuator;
import com.thinkit.core.annotation.Actuator;
import com.thinkit.core.constant.Channel;
import com.thinkit.nosql.base.BaseSolrService;
import com.thinkit.nosql.enums.SolrCoreEnum;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.utils.Checker;
import com.thinkit.utils.utils.SpringContextHolder;
import lombok.Getter;
import lombok.Setter;
import org.quartz.JobExecutionContext;

import java.util.List;
import java.util.Map;

@Actuator(code ="SOLR_DEL",name = "SOLR内容删除执行器")
public class SolrDelActuator extends TmpActuator {

    @Getter
    @Setter
    private ActionEnum actionEnum;

    private BaseSolrService solrService;

    private SolrConvert solrConvert;

    public SolrDelActuator(){
        super();
    }

    public SolrDelActuator(Object  params,ActionEnum actionEnum){
        super();
        this.actionEnum=actionEnum;
        this.solrService = SpringContextHolder.getBean(BaseSolrService.class);
        this.solrConvert = SpringContextHolder.getBean(SolrConvert.class);
    }

    @Override
    public void doFilter(Object params, FilterChain chain) {
        if(isNotJob()) {
            Object param =solrConvert.convertParam(params,actionEnum,this);
            if(null!=param && param instanceof ContentDto){
                ContentDto content =(ContentDto) param;
                if(Checker.BeNotNull(content.getRealDel())){
                    if(content.getRealDel()){
                        solrService.deleteByPk(SolrCoreEnum.DEFAULT_CORE,content.getId());
                    }else{
                        solrService.updateField(SolrCoreEnum.DEFAULT_CORE,content.getId(),"status","0");
                    }
                }
            }
        }else{
            chain.addJobFilter(this);
        }
        chain.doFilter(params,chain);
    }


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext)  {
        Map dateMap = jobExecutionContext.getJobDetail().getJobDataMap();

    }


}
