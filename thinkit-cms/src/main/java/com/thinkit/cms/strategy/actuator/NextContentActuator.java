package com.thinkit.cms.strategy.actuator;

import com.thinkit.cms.dto.content.PublishDto;
import com.thinkit.cms.strategy.filter.FilterChain;
import com.thinkit.cms.strategy.filter.TmpActuator;
import com.thinkit.core.annotation.Actuator;
import com.thinkit.core.constant.Channel;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import org.quartz.JobExecutionContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  查找当前文章的下一篇文章再次生成
 */
@Actuator(code ="NEXT_CONTENT_DETAIL_ACTUATOR",name = "下一篇内容详情模板生成执行器")
public class NextContentActuator extends TmpActuator  {

    @Getter @Setter
    private Object params;

    @Getter @Setter
    private ActionEnum actionEnum;

    public NextContentActuator(){
        super();
    }

    public NextContentActuator(Object params,ActionEnum actionEnum){
       super();
       this.params = params;
       this.actionEnum=actionEnum;
    }

    @Override
    public void doFilter(Object params, FilterChain chain) {
        if(Checker.BeNull(params)){
            params = this.params;
        }
        List<Map> buildParams = buildParams(this.params);
        if(Checker.BeNotEmpty(buildParams)){
            for(Map param:buildParams){
                if(isNotJob()) {
                    messageSend.sendMessage(ChannelEnum.CONTENT, Channel.NOTIFY_IT,param);
                }else{
                    chain.addJobFilter(this);
                }
            }
        }
        chain.doFilter(params,chain);
    }


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext)  {
        Map dateMap = jobExecutionContext.getJobDetail().getJobDataMap();
        List<Map> buildParams = buildParams(dateMap);
        if(Checker.BeNotEmpty(buildParams)){
            for(Map param:buildParams){
                messageSend.sendMessage(ChannelEnum.CONTENT, Channel.NOTIFY_IT,param);
            }
        }
    }


    private List<Map> buildParams( Object params){
        List<Map> result = new ArrayList<>();
        if(Checker.BeNotNull(params)){
            if(params instanceof PublishDto){
                PublishDto publishDto = (PublishDto) params;
                List<String> ids = publishDto.getIds();
                if(Checker.BeNotEmpty(ids)){
                    for(String id:ids){
                        Map<String,Object> map = new HashMap<>();
                        String categoryId = contentService.queryCategoryId(id);
                        Map<String,Object> next= contentService.nextPrevious(id,true,categoryId);
                        map.put(Channel.CATEGORY_ID,next.get("categoryId"));
                        map.put(Channel.MODEL_ID,next.get("modelId"));
                        map.put(Channel.CONTENT_ID,next.get("id"));
                        result.add(map);
                    }
                }
            }
        }
        return result;
    }

    public Object getParam(){
        return this.params;
    }


}
