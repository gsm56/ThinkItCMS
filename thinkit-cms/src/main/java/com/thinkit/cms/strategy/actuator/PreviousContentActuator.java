package com.thinkit.cms.strategy.actuator;

import com.thinkit.cms.strategy.convers.ContentConvert;
import com.thinkit.cms.strategy.filter.FilterChain;
import com.thinkit.cms.strategy.filter.TmpActuator;
import com.thinkit.core.annotation.Actuator;
import com.thinkit.core.constant.Channel;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.utils.Checker;
import com.thinkit.utils.utils.SpringContextHolder;
import lombok.Getter;
import lombok.Setter;
import org.quartz.JobExecutionContext;

import java.util.List;
import java.util.Map;

/**
 * 查找当前文章的上一篇文章再次生成
 */
@Actuator(code ="PREVIOUS_CONTENT_DETAIL_ACTUATOR",name = "上一篇内容详情模板生成执行器")
public class PreviousContentActuator extends TmpActuator  {

    @Getter @Setter
    private Object params;

    @Getter @Setter
    private ActionEnum actionEnum;

    private ContentConvert contentConvert;


    public PreviousContentActuator(){
        super();
    }

    public PreviousContentActuator(Object params,ActionEnum actionEnum){
       super();
       this.params = params;
       this.actionEnum =actionEnum;
       this.contentConvert = SpringContextHolder.getBean(ContentConvert.class);
    }

    @Override
    public void doFilter(Object params, FilterChain chain) {
        if(Checker.BeNull(params)){
            params = this.params;
        }
        List<Map<String,Object>> maps =contentConvert.convertParam(params,actionEnum,this);
        if(!maps.isEmpty()){
            for(Map map:maps){
                if(!map.isEmpty()){
                    String contentId = map.get(Channel.CONTENT_ID).toString();
                    String categoryId = map.get(Channel.CATEGORY_ID).toString();
                    Map<String,Object> prev= contentService.nextPrevious(contentId,false,categoryId);
                    if(Checker.BeNotEmpty(prev)){
                        prev.put(Channel.CATEGORY_ID,prev.get(Channel.CATEGORY_ID).toString());
                        prev.put(Channel.MODEL_ID,prev.get(Channel.MODEL_ID).toString());
                        prev.put(Channel.CONTENT_ID,prev.get(Channel.ID).toString());
                        messageSend.sendMessage(ChannelEnum.CONTENT, Channel.NOTIFY_IT,prev);
                    }
                }
            }
        }
        chain.doFilter(params,chain);
    }



    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext)  {
    }



    public Object getParam(){
        return this.params;
    }
}
