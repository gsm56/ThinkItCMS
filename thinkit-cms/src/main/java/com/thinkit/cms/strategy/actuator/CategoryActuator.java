package com.thinkit.cms.strategy.actuator;

import com.thinkit.cms.strategy.convers.CategoryConvert;
import com.thinkit.cms.strategy.filter.FilterChain;
import com.thinkit.cms.strategy.filter.TmpActuator;
import com.thinkit.core.annotation.Actuator;
import com.thinkit.core.constant.Channel;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.utils.Checker;
import com.thinkit.utils.utils.SpringContextHolder;
import lombok.Getter;
import lombok.Setter;
import org.quartz.JobExecutionContext;

import java.util.Map;

@Actuator(code ="CATEGORY_ACTUATOR",name = "生成栏目模板首页")
public class CategoryActuator extends TmpActuator {

    @Getter
    @Setter
    private Object params;

    @Getter @Setter
    private ActionEnum actionEnum;

    private CategoryConvert categoryConvert;

    public CategoryActuator(){
        super();
    }

    public CategoryActuator(Object params,ActionEnum actionEnum){
        super();
        this.params = params;
        this.actionEnum = actionEnum;
        this.categoryConvert = SpringContextHolder.getBean(CategoryConvert.class);
    }

    @Override
    public void doFilter(Object params, FilterChain chain) {
        if(Checker.BeNull(params)){
            params = this.params;
        }
        String categoryId = categoryConvert.convertParam(params,actionEnum);
        if(Checker.BeNotBlank(categoryId)){
            if(isNotJob()){
                messageSend.sendMessage(ChannelEnum.CATEGORY,Channel.NOTIFY_IT,categoryId);
            }else{
                chain.addJobFilter(this);
            }
        }
        chain.doFilter(params,chain);
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext)  {
        Map dateMap = jobExecutionContext.getJobDetail().getJobDataMap();
        String categoryId = categoryConvert.convertParam(dateMap,actionEnum);;
        messageSend.sendMessage(ChannelEnum.CATEGORY,Channel.NOTIFY_IT,categoryId);
    }


    public Object getParam(){
        return this.params;
    }
}
