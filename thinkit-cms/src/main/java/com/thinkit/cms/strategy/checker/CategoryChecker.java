package com.thinkit.cms.strategy.checker;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.StatusEnum;
import com.thinkit.utils.utils.Checker;
import lombok.Getter;
import lombok.Setter;

public class CategoryChecker implements ActuatorCheck {

    @Getter @Setter
    private CategoryDto category;

    @Getter @Setter
    private ActionEnum actionEnum;


    public CategoryChecker(Object arg){
        if(Checker.BeNotNull(arg)){
            CategoryDto category = (CategoryDto) arg;
            this.category =category;
        }
    }

    @Override
    public boolean executeFilter(Object arg) {
        if(Checker.BeNotNull(category)){
            if(actionEnum.equals(ActionEnum.UPDATE_CATEGORY)){ // 修改栏目
                return !category.getHidden();
            }else if(actionEnum.equals(ActionEnum.CREATE_CATEGORY)){// 创建栏目
                return !category.getHidden();
            }
        }
        return false;
    }

    @Override
    public void setAction(ActionEnum actionEnum) {
        this.actionEnum = actionEnum;
    }

    @Override
    public Object getParam() {
        return category;
    }
}
