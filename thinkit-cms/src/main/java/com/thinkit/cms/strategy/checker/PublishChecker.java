package com.thinkit.cms.strategy.checker;

import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.StatusEnum;
import com.thinkit.utils.utils.Checker;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class PublishChecker implements ActuatorCheck {

    @Getter @Setter
    private List<String> contentIds;

    @Getter @Setter
    private List<ContentDto> contents;

    @Getter @Setter
    private   String status;

    @Getter @Setter
    private ActionEnum actionEnum;


    public PublishChecker(String status, List<String> contentIds,ActionEnum actionEnum){
        if(Checker.BeNotBlank(status) && Checker.BeNotEmpty(contentIds)){
            this.status = status;
            this.contentIds =contentIds;
            this.actionEnum = actionEnum;
        }
    }

    public PublishChecker(String status,  List<String> contentIds,ActionEnum actionEnum,List<ContentDto> contents){
        if(Checker.BeNotBlank(status) && Checker.BeNotEmpty(contents)){
            this.status = status;
            this.contents =contents;
            this.contentIds =contentIds;
            this.actionEnum = actionEnum;
        }
    }

    @Override
    public boolean executeFilter(Object arg) {
        if(Checker.BeNotBlank(status)){
            if(actionEnum.equals(ActionEnum.UPDATE_CONTENT)){ // 修改时的判断依据
                return StatusEnum.PUBLISH.getCode().equals(status);
            }else if(actionEnum.equals(ActionEnum.DELETE_CONTENT)){// 删除时的判断依据
                return StatusEnum.PUBLISH.getCode().equals(status);
            }else if(actionEnum.equals(ActionEnum.PUBLISH_CONTENT)){// 发布时的判断依据
                return true;
            }
        }
        return false;
    }

    @Override
    public void setAction(ActionEnum actionEnum) {
        this.actionEnum = actionEnum;
    }

    @Override
    public Object getParam() {
        return contentIds;
    }

    public String getStatus() {
        return this.status;
    }

    public List<ContentDto> getContents() {
        return this.contents;
    }
}
