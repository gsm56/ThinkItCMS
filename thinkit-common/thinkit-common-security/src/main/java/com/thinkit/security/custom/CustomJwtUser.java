package com.thinkit.security.custom;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Sets;
import com.thinkit.utils.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Set;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
public class CustomJwtUser extends User {

    private String userId;

    private String orgId;

    private String userAccount;

    private Set<String> roleSigns;

    public CustomJwtUser(String userAccount, String password, String roleSign,Collection<GrantedAuthority> authorities){
        super(userAccount,password,authorities);
        this.userAccount = userAccount;
        this.roleSigns = roleSign(roleSign);
    }

    public CustomJwtUser(String userId,String orgId,String userAccount, String password, String roleSign,Collection<GrantedAuthority> authorities){
        super(userAccount,password,authorities);
        this.userAccount = userAccount;
        this.roleSigns = roleSign(roleSign);
        this.userId = userId;
        this.orgId = orgId;
    }

    private Set<String> roleSign(String roleSign){
        if(Checker.BeNotBlank(roleSign)){
           return CollUtil.newHashSet(StrUtil.split(roleSign,","));
        }
        return Sets.newHashSet();
    }


}
