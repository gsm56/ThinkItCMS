package com.thinkit.directive.components;

import freemarker.template.Configuration;

import java.util.Map;

public interface DirectiveInterceptor {

   Configuration injecGlobalVariable(Map<String, Object> params);

   Configuration injecShareVariable(Map<String, Object> params);
}
