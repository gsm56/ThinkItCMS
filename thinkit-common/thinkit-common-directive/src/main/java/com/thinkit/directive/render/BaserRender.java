package com.thinkit.directive.render;
import freemarker.template.TemplateModel;

import java.util.Map;
/**
 *模板渲染 基类 用于包装 模板指令参数封装等
 */
public interface BaserRender {

     void render() throws Exception;

     String getString(String name) throws Exception;

     String getString(String name,String defaultVal) throws Exception;

     Integer getInteger(String name) throws Exception;

     Integer getInteger(String name,Integer defaultVal) throws Exception;

     Long getLong(String name) throws Exception;

     Long getLong(String name,Long defaultVal) throws Exception;

     Short getShort(String name) throws Exception;

     Short getShort(String name,Short defaultVal) throws Exception;

     Double getDouble(String name) throws Exception;

     Double getDouble(String name,Double defaultVal) throws Exception;

     String[] getStringArray(String name) throws Exception;

     Long[] getLongArray(String name) throws Exception;

     Integer[] getIntegerArray(String name) throws Exception;

     Float getFloat(String name) throws Exception;

     Float getFloat(String name,Float defaultVal) throws Exception;

     Boolean getBoolean(String name) throws Exception;

     Boolean getBoolean(String name,Boolean defaultVal) throws Exception;

     <T>T getBean(String name,Class<T> clz) throws Exception;

     TemplateModel getMap(String name) throws Exception;

     BaserRender putAll(Map  maps);

     BaserRender put(String key  ,Object value);
}