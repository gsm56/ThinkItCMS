package com.thinkit.processor.job;

import lombok.Getter;

public enum JobActionNotify {
    CONTENT_PUBLISH ("PUBLISH_JOB","GROUP1"),
    INDEX_ACTUATOR ("INDEX_ACTUATOR","GROUP1");

    @Getter
    private String jobName;

    @Getter
    private String group;


    JobActionNotify(String jobName,String group) {
        this.jobName = jobName;
        this.group = group;
    }

}
