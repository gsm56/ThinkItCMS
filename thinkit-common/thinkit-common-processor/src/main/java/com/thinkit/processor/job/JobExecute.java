package com.thinkit.processor.job;

import com.thinkit.utils.utils.Checker;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Component
public class JobExecute  {

    @Autowired
    Scheduler scheduler;



    public void execute(String jobName ,String jobGroup, String triggerCron, Class<? extends  QuartzJobBean> clz, Map params) {
        synchronized (this) {
            Trigger trigger = jobTrigger(scheduler, jobName,jobGroup);
            if(Checker.BeNull(trigger)){
                JobUtil.createJobByCron(scheduler, jobName,jobGroup,triggerCron,clz,params);
            }else{
                JobUtil.modifyJobParam(scheduler,trigger,params);
            }
        }
    }

    public void execute(JobActionNotify jobActionNotify, String triggerCron, Class<? extends  QuartzJobBean> clz, Map params) {
        synchronized (this) {
            Trigger trigger = jobTrigger(scheduler, jobActionNotify);
            if(Checker.BeNull(trigger)){
                JobUtil.createJobByCron(scheduler,triggerCron, jobActionNotify.getJobName(),jobActionNotify.getGroup(),clz,params);
            }else{
                JobUtil.modifyJobParam(scheduler,trigger,params);
            }
        }
    }


    public void execute(JobActionNotify jobActionNotify, Date date, Class<? extends  QuartzJobBean> clz, Map params) {
        synchronized (this) {
            Trigger trigger = jobTrigger(scheduler, jobActionNotify);
            if(Checker.BeNull(trigger)){
                JobUtil.createJobByStartAt(scheduler,date.getTime(), jobActionNotify.getJobName(),jobActionNotify.getGroup(),clz,params);
            }else{
                JobUtil.modifyJobParam(scheduler,trigger,params);
            }
        }
    }

    public void execute(JobActionNotify jobActionNotify, Date date, Class<? extends  QuartzJobBean> clz, Map params,JobExecuteHandler jobExecuteHandler) {
        synchronized (this) {
            Trigger trigger = jobTrigger(scheduler, jobActionNotify);
            if(Checker.BeNull(trigger)){
                JobUtil.createJobByStartAt(scheduler,date.getTime(), jobActionNotify.getJobName(),jobActionNotify.getGroup(),clz,params);
            }else{
                boolean isExecu = jobExecuteHandler.before(scheduler,trigger,params);
                if(isExecu){
                    JobUtil.modifyJobParam(scheduler,trigger,params);
                }
            }
            jobExecuteHandler.after(scheduler,trigger,params);
        }
    }



    protected boolean triggerIsExist(Scheduler scheduler,JobActionNotify jobActionNotify){
        Trigger trigger= jobTrigger(scheduler,jobActionNotify);
        return Checker.BeNotNull(trigger);
    }

    protected Trigger jobTrigger(Scheduler scheduler,JobActionNotify jobActionNotify){
        Trigger trigger=JobUtil.ckJobIsExist(scheduler,jobActionNotify.getJobName(),jobActionNotify.getGroup());
        return trigger;
    }

    protected Trigger jobTrigger(Scheduler scheduler,String jobName,String jobGroup){
        Trigger trigger=JobUtil.ckJobIsExist(scheduler,jobName,jobGroup);
        return trigger;
    }
}
