package com.thinkit.processor.message;

import com.thinkit.processor.channel.BaseChannelService;

public interface MessageResolve extends Runnable{

     String channel();

     void channelService(BaseChannelService channelService);
}
