package com.thinkit.processor.message;
import com.thinkit.core.base.BaseContextKit;
import com.thinkit.core.constant.Channel;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Component
public class MessageSend {

    @Autowired
    RedisTemplate redisTemplate;


    public void sendMessage(ChannelEnum channelEmun, String method, Object... args){
        if(Checker.BeNotBlank(channelEmun.getCode())&&Checker.BeNotBlank(method)){
            redisTemplate.convertAndSend(channelEmun.getCode(),msg(method,args));
        }
    }

    public void sendMessage(ChannelEnum channelEmun,String method){
        redisTemplate.convertAndSend(channelEmun.getCode(),msg(method,null));
    }

    public void sendMessage(ChannelEnum[] channels,String method, Object... args){
        if(channels!=null && channels.length>0){
            for(ChannelEnum channel: channels){
                if(Checker.BeNotNull(args)){
                    sendMessage(channel,method,args);
                }
                else{
                    sendMessage(channel,method);
                }
            }
        }
    }

    public void sendMessage(ChannelEnum[] channels,String[] methods, Object... args){
        boolean notify = channels!=null && channels.length>0 && methods!=null && methods.length>0 &&
        channels.length == methods.length;
        if( notify ){
            for(int i=0;i<channels.length;i++){
                if(Checker.BeNotNull(args)){
                    sendMessage(channels[i],methods[i],args);
                }
                else{
                    sendMessage(channels[i],methods[i]);
                }
            }
        }
    }



    private Map<String,Object> msg(String method,Object[] args){
        Map<String,Object> msgMap = new HashMap<>();
        msgMap.put(Channel.METHOD,method);
        if(Checker.BeNotNull(args)){
            msgMap.put(Channel.ARGS,args);
        }
        String siteId = BaseContextKit.getSiteId();
        if(Checker.BeNotBlank(siteId)){
            msgMap.put(Channel.SITE_ID,siteId);
        }
        String userId = BaseContextKit.getUserId();
        if(Checker.BeNotBlank(userId)){
            msgMap.put(Channel.USER_ID,userId);
        }
        msgMap.put(Channel.DOMAIN,domain());
        return msgMap;
    }

    private String domain(){
         RequestAttributes requestAttributes=RequestContextHolder.getRequestAttributes();
         if(Checker.BeNotNull(requestAttributes)){
             HttpServletRequest request =((ServletRequestAttributes) requestAttributes).getRequest();
             return request.getServerName();
         }
         return null;
    }
}
