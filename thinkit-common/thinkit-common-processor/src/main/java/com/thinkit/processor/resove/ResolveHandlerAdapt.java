package com.thinkit.processor.resove;

import com.thinkit.core.constant.Channel;
import com.thinkit.processor.message.MessageResolve;
import com.thinkit.utils.utils.Checker;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public abstract class ResolveHandlerAdapt implements MessageResolve {

    private Object message;

    @Getter @Setter
    private String siteId;

    @Getter @Setter
    private String userId;

    public ResolveHandlerAdapt(Object message){
        if(Checker.BeNotNull(message)){
            this.message = message;
        }
        this.siteId = getString(Channel.SITE_ID);
        this.userId = getString(Channel.USER_ID);
    }

    protected String getString(String key){
        if(message instanceof Map){
            Object data = geMap().get(key);
            if(Checker.BeNotNull(data)){
                return data.toString();
            }
        }
        return null;
    }

    protected String getMethod(){
       if(message instanceof Map){
           Object method = geMap().get(Channel.METHOD);
           if(Checker.BeNotNull(method)){
               return method.toString();
           }
       }
       return null;
    }

    protected Object[] getArgs(){
        if(message instanceof Map){
            Object args = geMap().get(Channel.ARGS);
            if(Checker.BeNotNull(args)){
                if(args instanceof Object[]){
                    return (Object[]) args;
                }
            }
        }
        return null;
    }

    protected Map geMap(){
        return result();
    }

    private <T> T result(){
        if(Checker.BeNotNull(message)){
            return (T) message;
        }
        return null;
    }
}
