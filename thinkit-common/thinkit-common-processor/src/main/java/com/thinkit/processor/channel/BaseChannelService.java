package com.thinkit.processor.channel;

import com.thinkit.directive.components.AbstractNotify;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.model.ApiResult;

import java.util.List;
import java.util.Map;

public interface BaseChannelService{
    ChannelEnum getName();
    /**
     * 管道通知
     * @param id : 可代表任意ID ru: content channel 就代表 content id 其他 代表各自的 id
     */
    void notifyIt(String id);

    void notifyIt(String id,Boolean notify);

    void notifyIt();

    void notifyIt( Boolean notify);

    void notifyIt(Map<String,Object> params);

    void notifyIt(List params);

    void notifyIt(Map<String,Object> params,Boolean notify);

    ApiResult execuate();

    ApiResult execuate(Map<String,Object> variable);

    ApiResult execuate(AbstractNotify notify);

    void execuate(boolean aftercall);

    void execuate(boolean aftercall,boolean notify);

    void execuate(boolean aftercall,String... variableName);

    void execuate(AbstractNotify notify,boolean callBack);

    void callBack(Object param);

    BaseChannelService clone(boolean singleton) throws CloneNotSupportedException;
}
