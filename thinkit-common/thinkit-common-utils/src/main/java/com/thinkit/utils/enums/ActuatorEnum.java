package com.thinkit.utils.enums;
import lombok.Getter;

public enum ActuatorEnum {
	CONTENT_DETAIL_ACTUATOR("CONTENT_DETAIL_ACTUATOR","内容详情模板生成执行器"),
	CATEGORY_ACTUATOR("CATEGORY_ACTUATOR","生成栏目模板首页"),
	CATEGORY_PAGE_ACTUATOR("CATEGORY_PAGE_ACTUATOR","生成栏目分页模板页"),
	INDEX_ACTUATOR("INDEX_ACTUATOR","首页模板生成执行器"),
	NEXT_CONTENT_DETAIL_ACTUATOR("NEXT_CONTENT_DETAIL_ACTUATOR","下一篇内容详情模板生成执行器"),
	PREVIOUS_CONTENT_DETAIL_ACTUATOR("PREVIOUS_CONTENT_DETAIL_ACTUATOR","上一篇内容详情模板生成执行器"),
	SOLR_STORE("SOLR_STORE","同步内容到SOLR库"),
	SOLR_DELETE("SOLR_DEL","从SOLR库删除内容");


	@Getter
	private String code;

	@Getter
	private String name;

	ActuatorEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public static ActuatorEnum getActuatorEnum(String code) {
		for (ActuatorEnum each : ActuatorEnum.class.getEnumConstants()) {
			if (code.equals(each.code)) {
				return each;
			}
		}
		return null;
	}

	public static String getActuatorName(String code) {
		return getActuatorEnum(code).getName();
	}
}
