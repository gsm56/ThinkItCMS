package com.thinkit.utils.enums;
import lombok.Getter;

public enum UserFrom {
    MEMBER_USER("mem_user", "会员", "memUserDetailsService",
            "com.thinkit.security.login.MemLoginCheckerExecutor"),
    
    PLAT_USER("plat_user", "平台管理用户","platUserDetailsService",
"com.thinkit.security.login.PlatLoginCheckerExecutor");


    @Getter
    private String name;
    @Getter
    private String clientId;

    @Getter
    private String userService;

    @Getter
    private String loginCheckPck;

    UserFrom(String clientId, String name) {
        this.clientId = clientId;
        this.name = name;
    }

    UserFrom(String clientId, String name,String userService) {
        this.clientId = clientId;
        this.name = name;
        this.userService =userService;
    }

    UserFrom(String clientId, String name,String userService,String loginCheckPck) {
        this.clientId = clientId;
        this.name = name;
        this.userService =userService;
        this.loginCheckPck = loginCheckPck;
    }

    public static UserFrom getUserFrom(String clientId) {
        for (UserFrom each : UserFrom.class.getEnumConstants()) {
            if (clientId.equals(each.clientId)) {
                return each;
            }
        }
        return null;
    }

    public static boolean ckIsPlatUser(UserFrom userFrom){
        return userFrom.equals(PLAT_USER);
    }
}
