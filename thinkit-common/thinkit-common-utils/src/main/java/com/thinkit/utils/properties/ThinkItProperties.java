package com.thinkit.utils.properties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.io.File;

@Data
@Component
@ConfigurationProperties(prefix = "thinkit")
public class ThinkItProperties {

    public ThinkItProperties.path path = new ThinkItProperties.path();

    private String gateway;

    private String license;

    private String resourceServer;

    private String render="freemarker";

    private final String  suf ="/thinkitcmsresource/";

    public String getRootPath(){
        return this.path.getRootPath();
    }

    public String getSitePath(){
        String sitePath = getRootPath()+File.separator+this.path.getSite().getPathName();
        return sitePath.replace("\\","/");
    }

    public String getSourcePath(){
         String sourcePath =  getRootPath()+File.separator+this.path.getSource().getPathName();
         return sourcePath.replace("\\","/");
    }

    public String getTemplate(){
        String tempPath= getSourcePath()+File.separator+this.path.getSource().getTemplates();
        return tempPath.replace("\\","/");
    }

    public String getFileServerIp(){
        return this.path.getSource().getFileServerIp();
    }

    public String getFileBaseUrl(){
        return this.path.getSource().getFileServerIp()+ suf;
    }

    public String getFileLocalUrl(String filePath){

        if(filePath.contains(suf)){
            return getSourcePath()+filePath;
        }else{
            return getSourcePath()+suf+filePath;
        }
    }

    public String getFileLocalUrl(String filePath,String domain){
        if(filePath.contains(suf)){
            return getSourcePath()+"/"+domain+"/"+filePath;
        }else{
            return getSourcePath()+suf+domain+"/"+filePath;
        }
    }

    @Data
    public static class path{


        public ThinkItProperties.path.site site = new ThinkItProperties.path.site();

        public ThinkItProperties.path.source source = new ThinkItProperties.path.source();

        private String rootPath;

        @Data
        public static class site{
            /**
             * 网站跟路径名称
             */
            private String pathName;

        }

        @Data
        public static class source{

            /**
             * 资源跟路径名称
             */
            private String pathName;

            /**
             * 网站模板跟目录
             */
            private String templates;

            /**
             * 文件服务器 地址（可以为域名）
             */
            private String fileServerIp;


            /**
             * 文件服务器类型 ： oss,local fastdfs
             */
            private String fileServer;
        }
    }
}
