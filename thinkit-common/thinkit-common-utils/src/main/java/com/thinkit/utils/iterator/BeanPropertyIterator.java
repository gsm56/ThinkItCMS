package com.thinkit.utils.iterator;
import com.baomidou.mybatisplus.annotation.TableField;
import com.thinkit.utils.model.BaseModel;
import com.thinkit.utils.utils.Checker;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class BeanPropertyIterator implements ThinkIterator {

    public BeanPropertyBox beanPropertyBox;

    private int i=0;


    Map<String,Object> param=new LinkedHashMap<>(16);

    public BeanPropertyIterator(BeanPropertyBox beanPropertyBox){
        this.beanPropertyBox=beanPropertyBox;
    }

    @Override
    public boolean hasNext() {
        return i<beanPropertyBox.size();
    }

    @Override
    public Object next() {
        Object obj=beanPropertyBox.getBean();
        Field field= beanPropertyBox.getField(i);
        field.setAccessible(true);
        try {
            Object value=field.get(obj);
            if(isInvalid(value) || (obj instanceof BaseModel && isIgnoreField(field))){
                i++;
                return null;
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        i++;
        return field;
    }

    private boolean isInvalid(Object value){
        if(Checker.BeNull(value)){
            return true;
        }
        if(Checker.BeBlank(value.toString())){
            return true;
        }
        return  false;
    }

    private boolean isIgnoreField(Field field){
        TableField tableField=field.getAnnotation(TableField.class);
        if(Checker.BeNotNull(tableField)){
            if(!tableField.exist()){
                return true;
            }
        }else{
            return true;
        }
        return false;
    }
}
