package com.thinkit.utils.enums;

public enum ConditionType {

    EQ,NEQ,LIKE,NOT_LIKE,IN,NOT_IN;

}
