package com.thinkit.utils.enums;

import lombok.Getter;

public enum BelongEnum {
    MODEL("model", "模型"),
    CATEGORY("category", "分类扩展"),
    ATTACH("attach", "附件扩展"),
    FRAGMENT("fragment", "页面片段");

    @Getter
    private String name;
    @Getter
    private String code;

    BelongEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static BelongEnum getBelongEnum(String code) {
        for (BelongEnum each : BelongEnum.class.getEnumConstants()) {
            if (code.equals(each.code)) {
                return each;
            }
        }
        return null;
    }
}
